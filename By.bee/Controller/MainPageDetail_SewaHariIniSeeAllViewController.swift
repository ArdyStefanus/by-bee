//
//  MainPageDetail_SewaHariIniSeeAllViewController.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 06/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class MainPageDetail_SewaHariIniSeeAllViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource
{

    @IBOutlet weak var collectionViewSewa: UICollectionView!
    
    var tempSewa = ItemManager().items
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewSewa.delegate = self
        collectionViewSewa.dataSource = self

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempSewa.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seeAllSewaHariIdentifier", for: indexPath) as! SeeAllSewaHariIniCollectionViewCell
        
        cell.iconLoveSewa.image = UIImage (named: tempSewa[indexPath.row].imageLove)
        cell.imagePathSewa.image = UIImage (named: tempSewa[indexPath.row].imagePath)
        cell.textViewBulanSewa.text = "\(tempSewa[indexPath.row].minimumAgeInMonths ?? 2) - \(tempSewa[indexPath.row].maximumAgeInMonths ?? 4) Bulan"
        cell.textViewHarga.text = "\(tempSewa[indexPath.row].buyPrice.formattedWithSeparator)"
        cell.textViewKetSewa.text = tempSewa[indexPath.row].name
        cell.textViewTanggalSewa.text = "28 Juli | 2 Minggu"
        
        cell.layer.cornerRadius = 3
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false //<-
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
                let detailvc = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetail_ViewController
        detailvc.currentItem = tempSewa[indexPath.row]
    navigationController?.pushViewController(detailvc, animated: true)
    }
    
}

