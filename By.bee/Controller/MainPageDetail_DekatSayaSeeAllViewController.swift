//
//  MainPageDetail_DekatSayaSeeAllViewController.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 06/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class MainPageDetail_DekatSayaSeeAllViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource
{

    var tempAllNear = ItemManager().dekatSaya()
    
    @IBOutlet weak var collectionViewNear: UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        collectionViewNear.delegate = self
        collectionViewNear.dataSource = self
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempAllNear.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seeAllDekatSayaIdentifier", for: indexPath) as! SeeAllDekatSayaCollectionViewCell
        
        cell.iconLoveDekatSaya.image = UIImage (named: tempAllNear[indexPath.row].imageLove)
        cell.imageMainDekatSaya.image = UIImage (named: tempAllNear[indexPath.row].imagePath)
        cell.textViewBulan.text = "\(tempAllNear[indexPath.row].minimumAgeInMonths ?? 2) - \(tempAllNear[indexPath.row].maximumAgeInMonths ?? 4) Bulan"
        cell.textViewHarga.text = "\(tempAllNear[indexPath.row].buyPrice.formattedWithSeparator)"
        cell.textViewKeterangan.text = tempAllNear[indexPath.row].name
        cell.textViewTanggal.text = "28 Juli | 2 Minggu"
        cell.layer.cornerRadius = 3
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false //<-
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
                let detailvc = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetail_ViewController
        detailvc.currentItem = tempAllNear[indexPath.row]
    navigationController?.pushViewController(detailvc, animated: true)
    }
}
