//
//  MainPageDetail_SiKecilSeeAllViewController.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 05/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit
import Foundation

class MainPageDetail_SiKecilSeeAllViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource
{
    @IBOutlet weak var seeAllSiKecilCollectionView: UICollectionView!
    
    var tempAllSiKecil = ItemManager().siKecil()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        seeAllSiKecilCollectionView.delegate = self
        seeAllSiKecilCollectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempAllSiKecil.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seeAllSiKecilIdentifier", for: indexPath) as! SeeAllSiKecilCollectionViewCell
        
        cell.iconLoveSiKecil.image = UIImage (named: tempAllSiKecil[indexPath.row].imageLove)
        cell.imageMainSiKecil.image = UIImage (named: tempAllSiKecil[indexPath.row].imagePath)
        cell.textViewBulanSiKecil.text = "\(tempAllSiKecil[indexPath.row].minimumAgeInMonths ?? 2) - \(tempAllSiKecil[indexPath.row].maximumAgeInMonths ?? 4) Bulan"
        cell.textViewHargaSiKecil.text = "\(tempAllSiKecil[indexPath.row].buyPrice.formattedWithSeparator)"
        cell.textViewKeteranganSiKecil.text = tempAllSiKecil[indexPath.row].name
        cell.textViewTanggalSiKecil.text = "28 Juli | 2 Minggu"
        
        cell.layer.cornerRadius = 3
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false //<-
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
                let detailvc = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetail_ViewController
        detailvc.currentItem = tempAllSiKecil[indexPath.row]
    navigationController?.pushViewController(detailvc, animated: true)
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        return formatter
    }()
}

extension Numeric {
    var formattedWithSeparator: String { Formatter.withSeparator.string(for: self) ?? "" }
}
