//
//  CategoryDetailViewController.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 12/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class CategoryDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var collectionViewButton: UICollectionView!
    @IBOutlet weak var tableViewCategory: UITableView!
    
    let imageCategory = [#imageLiteral(resourceName: "role play"), #imageLiteral(resourceName: "manipulati ve play"), #imageLiteral(resourceName: "active play"), #imageLiteral(resourceName: "learning play"), #imageLiteral(resourceName: "role play")]
    
    var tempNameButton:[String] = ["0-6 Bulan", "7-11 Bulan", "1 Tahun", "2 Tahun", "3 Tahun", "4 Tahun", "5+ Tahun"]
    
    var tempTableNameCategory:[String] = ["Stimulating Toys", "Solving Toys", "Imaginative Toys", "Creative Toys", "Learning Toys"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        collectionViewButton.delegate = self
        collectionViewButton.dataSource = self
        
        tableViewCategory.delegate = self
        tableViewCategory.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempNameButton.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonIdentifier", for: indexPath) as! buttonCell
        
        cell.buttonText.setTitle("\(tempNameButton[indexPath.row])", for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        tempTableNameCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableViewCategory.dequeueReusableCell(withIdentifier: "tableCategoryIdentifier", for: indexPath) as! tableCellCategory
        
        cell.labelNameCategory.text = tempTableNameCategory[indexPath.row]
        cell.imageTableCategory.image = imageCategory[indexPath.row]
        
        cell.imageTableCategory.layer.cornerRadius = 10
        cell.imageTableCategory.layer.borderWidth = 0.0
        cell.imageTableCategory.layer.shadowColor = UIColor.black.cgColor
        cell.imageTableCategory.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.imageTableCategory.layer.shadowRadius = 3.0
        cell.imageTableCategory.layer.shadowOpacity = 0.2
        cell.imageTableCategory.layer.masksToBounds = false //<-
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 203
    }
}

class buttonCell: UICollectionViewCell
{
    @IBOutlet weak var buttonText: UIButton!
}

class tableCellCategory: UITableViewCell{
    @IBOutlet weak var labelNameCategory: UILabel!
    @IBOutlet weak var imageTableCategory: UIImageView!
}
