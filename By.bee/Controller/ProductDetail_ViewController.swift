//
//  ProductDetail_ViewController.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 13/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class ProductDetail_ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    
    @IBOutlet weak var imageProductDetail: UIImageView!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var buyPriceLabel: UILabel!
    @IBOutlet weak var nameProductDetail: UILabel!
    @IBOutlet weak var OtherCollectionView: UICollectionView!
    @IBOutlet weak var imageStoreProductDetail: UIImageView!
    @IBOutlet weak var nameStoreProductDetail: UILabel!
    @IBOutlet weak var ageGroupLabel: UILabel!
    @IBOutlet weak var categoryProductDetail: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    
    let itemManager = ItemManager()
    let storeManager = StoreManager()
    
    var otherItemsInStore: [Item] = []
    var currentStore: Store = Store(name: "Toko Yonatan", iconPath: "AppIcon", listedItemsID: [1,2,3])
//    let currentStore: Store = storeM
    var currentItem: Item = Item(id: 99, name: "Singing Animal Keyboard", category: "Creative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 500000, dimensions: [35, 35, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Singing Animal Keyboard", imageLove: "loveImage", jarakPengiriman: 4, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Biru", minimumAgeInMonths: 3, maximumAgeInMonths: 6)
    
        override func viewDidLoad()
        {
            super.viewDidLoad()
            OtherCollectionView.dataSource = self
            OtherCollectionView.delegate = self
            OtherCollectionView.backgroundColor = UIColor.white
            currentStore = storeManager.getStoreFromItemID(id: currentItem.id)
            setItemInformation()
            
           
            
            getOtherItemsInStore()
        }
    
    func setItemInformation()
    {
        imageProductDetail.image = UIImage (named: currentItem.imagePath)
        costLabel.text = "Rp\(currentItem.costs[1].formattedWithSeparator)"
        buyPriceLabel.text = "Harga beli: Rp. \(currentItem.buyPrice.formattedWithSeparator)"
        nameProductDetail.text = currentItem.name
        imageStoreProductDetail.image = UIImage (named: currentStore.iconPath)
        nameStoreProductDetail.text = "\(currentStore.name)"
        ageGroupLabel.text = "\(currentItem.minimumAgeInMonths!) Bulan+"
        categoryProductDetail.text = "\(currentItem.category)"
        sizeLabel.text = "\(currentItem.dimensions[0]) x \(currentItem.dimensions[1]) x \(currentItem.dimensions[2]) \(currentItem.dimensionUnit) (PxLxT)"
        colorLabel.text = currentItem.color
    }
    
    func getOtherItemsInStore(){
        print(currentStore)
        print(currentItem)
        for index in 0...currentStore.listedItemsID.count - 1 {
            
            let idToCheck: Int = currentStore.listedItemsID[index]
            let itemToCheck: Item = itemManager.getItemFromId(id: idToCheck)[0]
            if itemToCheck.id != currentItem.id {
                otherItemsInStore.append(itemToCheck)
            }
        }
        print(otherItemsInStore)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDetailCell", for: indexPath) as! ProductDetailCollectionViewCell
        
        
        cell.photo.image = UIImage (named: "\(otherItemsInStore[indexPath.row].imagePath!)")
        cell.ageGroup.text = "\(otherItemsInStore[indexPath.row].minimumAgeInMonths!) BULAN+"
        cell.name.text = otherItemsInStore[indexPath.row].name
        cell.price.text = "Rp\(otherItemsInStore[indexPath.row].costs[1].formattedWithSeparator)"
        cell.availability.text = "30 Aug | 2 Minggu"

        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.2
        cell.layer.masksToBounds = false
        
        return cell
    }

}
