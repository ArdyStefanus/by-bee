//
//  ViewController.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 29/07/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource
{
    @IBOutlet weak var kategoriCollectionView: UICollectionView!
    
    @IBOutlet weak var siKecilSukaCollectionView: UICollectionView!
    
    @IBOutlet weak var dekatSayaCollectionView: UICollectionView!
    
    @IBOutlet weak var sewaHariIniCollectionView: UICollectionView!
    
    let tempListCategory = ListCategory()
    
    let tempItems = ItemManager()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        let _: CGFloat = self.navigationController!.navigationBar.frame.height
        
        kategoriCollectionView.delegate = self
        kategoriCollectionView.dataSource = self
        
        siKecilSukaCollectionView.delegate = self
        siKecilSukaCollectionView.dataSource = self
        
        dekatSayaCollectionView.delegate = self
        dekatSayaCollectionView.dataSource = self
        
        sewaHariIniCollectionView.delegate = self
        sewaHariIniCollectionView.dataSource = self
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == kategoriCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "kategoriIdentifier", for: indexPath) as! kategoriCollectionViewCell
            cell.imageKategori.image = UIImage (named: tempListCategory.list[indexPath.row].imageCategory)
            cell.umurKategori.text = tempListCategory.list[indexPath.row].textCategory
            
            cell.layer.cornerRadius = 10
            cell.layer.borderWidth = 0
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false //<-
            
            return cell
        }
        
        else if collectionView == siKecilSukaCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "siKecilIdentifier", for: indexPath) as! siKecilCollectionViewCell
            
            cell.iconLoveSiKecil.image = UIImage (named: tempItems.siKecil()[indexPath.row].imageLove)
            cell.imageSiKecil.image = UIImage (named: tempItems.siKecil()[indexPath.row].imagePath)
            cell.textViewBulan.text = "\(tempItems.siKecil()[indexPath.row].minimumAgeInMonths ?? 2) - \(tempItems.siKecil()[indexPath.row].maximumAgeInMonths ?? 4) Bulan"
            cell.textViewKeterangan.text = tempItems.siKecil()[indexPath.row].name
            cell.textViewHarga.text = "Rp. \(tempItems.siKecil()[indexPath.row].buyPrice.formattedWithSeparator)"
            cell.textViewTanggal.text = "20 Juli | 2 minggu"
            
            cell.layer.cornerRadius = 10
            cell.layer.borderWidth = 0.0
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false //<-
            
            return cell
        }
        
        else if collectionView == dekatSayaCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dekatSayaIdentifier", for: indexPath) as! dekatSayaCollectionViewCell
            cell.iconLoveDekat.image = UIImage (named: tempItems.dekatSaya()[indexPath.row].imageLove)
            cell.imageDekat.image = UIImage (named: tempItems.dekatSaya()[indexPath.row].imagePath)
            cell.textViewBUlan.text = "\(tempItems.dekatSaya()[indexPath.row].minimumAgeInMonths ?? 2) - \(tempItems.dekatSaya()[indexPath.row].maximumAgeInMonths ?? 4) Bulan"
            cell.textViewKeterangan.text = tempItems.dekatSaya()[indexPath.row].name
            cell.textViewHarga.text = "Rp. \(tempItems.dekatSaya()[indexPath.row].buyPrice.formattedWithSeparator)"
            cell.textViewTanggal.text = "20 Juli | 2 minggu"
            
            cell.layer.cornerRadius = 10
            cell.layer.borderWidth = 0.0
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false //<-
            
            return cell
        }
        
        else if collectionView == sewaHariIniCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sewaHariIniIdentifier", for: indexPath) as! sewaHariIniCollectionViewCell
            cell.iconLoveSewa.image = UIImage (named: tempItems.items[indexPath.row].imageLove)
            cell.imageSewa.image = UIImage (named: tempItems.items[indexPath.row].imagePath)
            cell.textViewBulanSewa.text = "\(tempItems.items[indexPath.row].minimumAgeInMonths ?? 3) - \(tempItems.items[indexPath.row].maximumAgeInMonths ?? 6) Bulan"
            cell.textViewKeteranganSewa.text = tempItems.items[indexPath.row].name
            cell.textViewHargaSewa.text = "Rp. \(tempItems.items[indexPath.row].buyPrice.formattedWithSeparator)"
            cell.textViewTanggalSewa.text = "20 Juli | 2 minggu"
            
            cell.layer.cornerRadius = 10
            cell.layer.borderWidth = 0.0
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 0.2
            cell.layer.masksToBounds = false //<-
            
            return cell
        }
        
        return  collectionView.dequeueReusableCell(withReuseIdentifier: "kategoriIdentifier", for: indexPath) as! kategoriCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == siKecilSukaCollectionView
        {
            let detailvc = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetail_ViewController
            detailvc.currentItem = tempItems.siKecil()[indexPath.row]
            navigationController?.pushViewController(detailvc, animated: true)
        }
        
        else if collectionView == dekatSayaCollectionView{
            let detailvc = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetail_ViewController
            detailvc.currentItem = tempItems.dekatSaya()[indexPath.row]
            navigationController?.pushViewController(detailvc, animated: true)
        }
        
        else if collectionView == sewaHariIniCollectionView{
            let detailvc = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetail_ViewController
            detailvc.currentItem = tempItems.items[indexPath.row]
            navigationController?.pushViewController(detailvc, animated: true)
        }
    }
}





