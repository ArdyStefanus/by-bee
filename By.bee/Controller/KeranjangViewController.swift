//
//  KeranjangViewController.swift
//  By.bee
//
//  Created by Nicholas Ang on 11/08/2020.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class KeranjangViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cartItems = StoreManager().getItemsInCart()
    
    @IBOutlet weak var sewaButtonContainer: UIView!
    var tableGenerationSequence: [Any] = []
    var selectedItemCount: Int = 0
    var totalPrice: Int = 0
    var totalRow: Int = 0
    
    
    @IBOutlet weak var totalHargaLabel: UILabel!
    @IBOutlet weak var sewaButton: UIButton!
    @IBOutlet weak var keranjangTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keranjangTableView.delegate = self
        keranjangTableView.dataSource = self
        setCellsToLoad()
        
        let tabBarHeight = (self.tabBarController?.tabBar.frame.height)!
        let containerYPos = view.frame.height - tabBarHeight - 35
        sewaButtonContainer.center = CGPoint (x: view.center.x, y: containerYPos)
//        let margins = view.layoutMarginsGuide
//        NSLayoutConstraint.activate([
//           sewaButtonContainer.bottomAnchor.constraint(equalTo: margins.bottomAnchor),
//           sewaButtonContainer.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
//           sewaButtonContainer.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
//           sewaButtonContainer.widthAnchor.constraint(equalToConstant: 70)
//        ])
//        sewaButtonContainer.center = CGPoint (x: view.center.x, y: view.safeAreaInsets.bottom - 35 )
        //        print("Reached")
        // Do any additional setup after loading the view.
    }
    
    
    func selectionUpdate(price: Int) {
        totalPrice += price
        totalHargaLabel.text = "Rp\(totalPrice.formattedWithSeparator)"
        
        if price > 0 {
            selectedItemCount += 1
        }
        else {
            selectedItemCount -= 1
        }
        
        sewaButton.setTitle("Sewa(\(selectedItemCount))", for: .normal)
//        sewaButton.setTitle(500000.formattedWithSeparator, for: .normal)
    }
    
   
    
    @IBAction func sewaButtonTap(_ sender: Any) {
        
    }
    
    func setCellsToLoad() {
        for storeIndex in 0...cartItems.count-1 {
            
            totalRow += 1
            tableGenerationSequence.append(cartItems[storeIndex].store)
            //            print(tableGenerationSequence)
            for itemIndex in 0...cartItems[storeIndex].items.count-1 {
                totalRow += 1
                let itemToAdd = cartItems[storeIndex].items[itemIndex]
                tableGenerationSequence.append(itemToAdd)
                selectionUpdate(price: itemToAdd.costs[itemToAdd.selectedDurationCode!])
//                print("Total Sequence \(tableGenerationSequence.count)")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return totalRow+1

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row < tableGenerationSequence.count {
            if tableGenerationSequence[indexPath.row] is Store {
                return 32
            }
            else {
                return 128
            }
        }
        else {
//            print("Collection Reached")
            return 323
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print (tableGenerationSequence.count)
        if indexPath.row < tableGenerationSequence.count {
            print(indexPath.row)
            print(tableGenerationSequence[indexPath.row])
            if tableGenerationSequence[indexPath.row] is Store {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StoreInCartCell", for: indexPath) as! TokoKeranjangTableViewCell
                let tempStore: Store = tableGenerationSequence[indexPath.row] as!
                Store
                
                cell.storeName.text = tempStore.name
                
                //            print("Generating Cell")
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ItemInCartCell", for: indexPath) as! KeranjangTableViewCell
                
                let tempItem: Item = tableGenerationSequence[indexPath.row] as! Item
                
                cell.itemImage.image = UIImage (named: tempItem.imagePath)
                cell.itemName.text = tempItem.name
                cell.itemPrice.text = String("Rp\(tempItem.costs[tempItem.selectedDurationCode!].formattedWithSeparator) (\(tempItem.numberInCart) barang)")
                cell.itemRentalDate.text = "21 Juli - 4 Agustus"
                //            print("Generating Cell")
                cell.delegate = self
                
                //            print(cell.delegate)
                return cell
            }
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ComplementaryInCartCell", for: indexPath) as! ComplementaryKeranjangTableViewCell
            
            return cell
        }
    }
}

extension KeranjangViewController: KeranjangTableViewCellDelegate {
    func didTapEdit() {
        print("Edit Button Tapped")
    }
    
    func didTapDelete() {
        print("Delete Button Tapped")
    }
}


//Contoh : 5000000.formattedWithSeparator
