//
//  ProductDetailCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 13/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class ProductDetailCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var ageGroup: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var availability: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
