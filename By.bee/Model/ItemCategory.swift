//
//  ItemCategory.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 07/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import Foundation

struct ItemCategory
{
    var imageCategory: String!
    var textCategory: String!
}

class ListCategory
{
    var list : [ItemCategory] = []
    init()
    {
        self.list = [
            ItemCategory(imageCategory: "0-6 Bulan", textCategory: "0-6 Bulan"),
            ItemCategory(imageCategory: "7-11 Bulan", textCategory: "7-11 Bulan"),
            ItemCategory(imageCategory: "1 Tahun", textCategory: "1 Tahun"),
            ItemCategory(imageCategory: "0-6 Bulan", textCategory: "2 Tahun"),
            ItemCategory(imageCategory: "7-11 Bulan", textCategory: "4 Tahun"),
            ItemCategory(imageCategory: "1 Tahun", textCategory: "5+ Tahun"),
        ]
    }
}
