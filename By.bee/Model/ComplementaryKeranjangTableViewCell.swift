//
//  ComplementaryKeranjangTableViewCell.swift
//  By.bee
//
//  Created by Nicholas Ang on 12/08/2020.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class ComplementaryKeranjangTableViewCell: UITableViewCell {
    
    @IBOutlet weak var complementaryCollection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        complementaryCollection.dataSource = self
        complementaryCollection.delegate = self
        complementaryCollection.backgroundColor = UIColor.white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension ComplementaryKeranjangTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("X")
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherStoreCell", for: indexPath) as! ProductDetailCollectionViewCell
        
        cell.photo.image = #imageLiteral(resourceName: "Superstar Keyboard and Stool")
        cell.ageGroup.text = "6 BULAN+"
        cell.name.text = "Meja Toyota - Pinky Minaj"
        cell.price.text = "Rp500.000"
        cell.availability.text = "30 Aug | 2 Minggu"
        print("Reached")
        cell.layer.cornerRadius = 3
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 0.15
        cell.layer.masksToBounds = false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let headerView = complementaryCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ComplementaryHeader", for: indexPath) as? ComplementaryCellHeader
                else {
                    fatalError("Invalid view type")
            }
            return headerView
        case UICollectionView.elementKindSectionFooter:
            guard let footerView = complementaryCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ComplementaryFooter", for: indexPath) as? ComplementaryCellHeader
                else {
                    fatalError("Invalid view type")
            }
            return footerView
        default:
            guard let headerView = complementaryCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ComplementaryHeader", for: indexPath) as? ComplementaryCellHeader
                          else {
                              fatalError("Invalid view type")
                      }
                      return headerView
            
        }
        
        
    }
}
