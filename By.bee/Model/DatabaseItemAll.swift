//
//  DatabaseItemAll.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 06/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import Foundation

struct All_item {
     var name: String!
     var category: String!
     var onRental: [Date]!
     var durationsInDays: [Int]!
     var costs: [Int]!
     var buyPrice: Int!
     var dimensions: [Int]!
     var dimensionUnit: String!
     enum unit:String {
            case cm, m
     }
     
     var rating : [Int]!
    
     var color: String!
     var minimumAgeInMonths: Int!
     var maximumAgeInMonths: Int!
    
    static func all() -> [All_item]
    {
        return
            [
                All_item(name: "Prusutan Toyota", category: "Active Play", onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>),
                
                All_item(name: <#T##String?#>, category: <#T##String?#>, onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>),
                
                All_item(name: <#T##String?#>, category: <#T##String?#>, onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>),
                
                All_item(name: <#T##String?#>, category: <#T##String?#>, onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>),
                
                All_item(name: <#T##String?#>, category: <#T##String?#>, onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>),
                
                All_item(name: <#T##String?#>, category: <#T##String?#>, onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>),
                
        ]
        All_item.init(name: <#T##String?#>, category: <#T##String?#>, onRental: <#T##[Date]?#>, durationsInDays: <#T##[Int]?#>, costs: <#T##[Int]?#>, buyPrice: <#T##Int?#>, dimensions: <#T##[Int]?#>, dimensionUnit: <#T##String?#>, rating: <#T##[Int]?#>, color: <#T##String?#>, minimumAgeInMonths: <#T##Int?#>, maximumAgeInMonths: <#T##Int?#>)
    }
    
    static func category() -> [All_item]
    {
        return self.all().filter
            {
                item in item.category == "Active Play"
        }
    }
}
