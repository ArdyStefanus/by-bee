//
//  SeeAllDekatSayaCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 06/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class SeeAllDekatSayaCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var iconLoveDekatSaya: UIImageView!
    @IBOutlet weak var imageMainDekatSaya: UIImageView!
    @IBOutlet weak var textViewBulan: UILabel!
    @IBOutlet weak var textViewKeterangan: UILabel!
    @IBOutlet weak var textViewHarga: UILabel!
    @IBOutlet weak var textViewTanggal: UILabel!
}
