//
//  siKecilCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 05/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class siKecilCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var iconLoveSiKecil: UIImageView!
    @IBOutlet weak var imageSiKecil: UIImageView!
    @IBOutlet weak var textViewBulan: UILabel!
    @IBOutlet weak var textViewKeterangan: UILabel!
    @IBOutlet weak var textViewHarga: UILabel!
    @IBOutlet weak var textViewTanggal: UILabel!
}
