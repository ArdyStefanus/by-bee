//
//  kategoriCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 05/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class kategoriCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageKategori: UIImageView!
    @IBOutlet weak var umurKategori: UILabel!
}
