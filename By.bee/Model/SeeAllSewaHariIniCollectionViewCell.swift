//
//  SeeAllSewaHariIniCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 06/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class SeeAllSewaHariIniCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var iconLoveSewa: UIImageView!
    @IBOutlet weak var imagePathSewa: UIImageView!
    @IBOutlet weak var textViewBulanSewa: UILabel!
    @IBOutlet weak var textViewKetSewa: UILabel!
    @IBOutlet weak var textViewHarga: UILabel!
    @IBOutlet weak var textViewTanggalSewa: UILabel!
}
