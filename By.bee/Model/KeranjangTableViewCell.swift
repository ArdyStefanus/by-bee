//
//  KeranjangTableViewCell.swift
//  By.bee
//
//  Created by Nicholas Ang on 11/08/2020.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

protocol KeranjangTableViewCellDelegate {
    func didTapEdit()
    func didTapDelete()
}

class KeranjangTableViewCell: UITableViewCell {

    var delegate: KeranjangTableViewCellDelegate?
    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemRentalDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        print ("Awake")
//         Initialization code
    }

    @IBAction func editItem(_ sender: Any) {
//        print("Y")
        delegate?.didTapEdit()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
