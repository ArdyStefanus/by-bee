//
//  Item.swift
//  By.bee
//
//  Created by Nicholas Ang on 05/08/2020.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import Foundation

struct Item {
    var id: Int
    var name: String
    var category: String
    var onRental: [Date]
    var durationsInDays: [Int]
    var costs: [Int]
    var buyPrice: Int
    var dimensions: [Int]
    var dimensionUnit: String
    var imagePath: String!
    var imageLove: String!
    var jarakPengiriman: Double
    var isKecil: Bool
    var selectedDurationCode : Int?
    var selectedStartingDate: Date?
    var numberInCart : Int
    
    enum unit:String {
           case cm, m
    }
   
    var color: String
    var minimumAgeInMonths: Int!
    var maximumAgeInMonths: Int!
}

class ItemManager
{
    var items: [Item] = []
    
    init() {
        
        items.append(Item(id: 1, name: "Activity Triangle", category: "Learning Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [50000, 70000, 85000], buyPrice: 370000, dimensions: [30, 30, 30], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Activity Triangle", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Merah Biru", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
        
        items.append(Item(id: 2, name: "Giant activity cube", category: "Learning Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [100000, 140000, 180000], buyPrice: 1170000, dimensions: [40, 40, 40], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Giant activity cube", imageLove: "loveImage", jarakPengiriman: 2, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 1, color: "Cokelat", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
        
        items.append(Item(id: 3, name: "Light and sound walker", category: "Learning Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 550000, dimensions: [30, 30, 20], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Light and sound walker", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 1, color: "Merah Hijau", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 4, name: "Little Senses Lights and Sounds music station", category: "Learning Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [100000, 140000, 180000], buyPrice: 12500000, dimensions: [40, 40, 30], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Little Senses Lights and Sounds music station", imageLove: "loveImage", jarakPengiriman: 4, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 1, color: "Merah Biru", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 5, name: "Wooden activity cube", category: "Learning Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [100000, 140000, 180000], buyPrice: 1170000, dimensions: [31, 33, 31], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Wooden activity cube", imageLove: "loveImage", jarakPengiriman: 3, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Cokelat", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
                items.append(Item(id: 6, name: "Little Senses Lights and sounds music station", category: "Creative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 500000, dimensions: [36, 20, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Little Senses Lights and sounds music station", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Merah", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
                items.append(Item(id: 7, name: "Singing Animal Keyboard", category: "Creative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 500000, dimensions: [35, 35, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Singing Animal Keyboard", imageLove: "loveImage", jarakPengiriman: 4, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Biru", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
        
                items.append(Item(id: 8, name: "Superstar cool guitar", category: "Creative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [80000, 110000, 140000], buyPrice: 550000, dimensions: [10, 6, 5], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Superstar cool guitar", imageLove: "loveImage", jarakPengiriman: 8, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Merah", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
                items.append(Item(id: 9, name: "Superstar Keyboard and Stool", category: "Creative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [120000, 160000, 200000], buyPrice: 1400000, dimensions: [10, 6, 5], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Superstar Keyboard and Stool", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Merah", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
                items.append(Item(id: 10, name: "Wooden music set", category: "Creative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 500000, dimensions: [30, 20, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "wooden music set", imageLove: "loveImage", jarakPengiriman: 2, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Pink", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
        
        items.append(Item(id: 11, name: "Cash Register", category: "Imaginative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 90000, 120000], buyPrice: 600000, dimensions: [31, 33, 31], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Cash Register", imageLove: "loveImage", jarakPengiriman: 5, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 1, color: "Cokelat", minimumAgeInMonths: 3, maximumAgeInMonths: 6))

        items.append(Item(id: 12, name: "Cleaning Trolley", category: "Imaginative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 500000, dimensions: [35, 35, 40], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Cleaning Trolley", imageLove: "loveImage", jarakPengiriman: 3, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Merah", minimumAgeInMonths: 2, maximumAgeInMonths: 4))

        items.append(Item(id: 13, name: "Drill and Learn Tool Box", category: "Imaginative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [90000, 130000, 160000], buyPrice: 800000, dimensions: [30, 40, 20], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Drill and Learn Tool Box", imageLove: "loveImage", jarakPengiriman: 2, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Abu-abu", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 14, name: "Rosebud Doll Treehouse", category: "Imaginative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [130000, 180000, 220000], buyPrice: 1300000, dimensions: [40, 40, 90], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Rosebud Doll Treehouse", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 1, color: "Pink", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 15, name: "Sing Along Star Microphone", category: "Imaginative Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [70000, 100000, 120000], buyPrice: 550000, dimensions: [10, 6, 5], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Sing Along Star Microphone", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Merah", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
        
                items.append(Item(id: 16, name: "Sand and Water Table", category: "Solving Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [150000, 200000, 250000], buyPrice: 14000000, dimensions: [40, 40, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Sand and Water Table", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Biru", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
                
                items.append(Item(id: 17, name: "Alphabet Teaching Frame", category: "Solving Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [45000, 65000, 80000], buyPrice: 400000, dimensions: [40, 40, 20], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Alphabet Teaching Frame", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Cokelat", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
                
                items.append(Item(id: 18, name: "Build it Starter Set", category: "Solving Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [45000, 65000, 80000], buyPrice: 400000, dimensions: [30, 20, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Build it Starter Set", imageLove: "loveImage", jarakPengiriman: 3, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Hijau", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
                
                items.append(Item(id: 19, name: "Car Rescue", category: "Solving Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [100000, 1400000, 180000], buyPrice: 1170000, dimensions: [40, 40, 20], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Car Rescue", imageLove: "loveImage", jarakPengiriman: 6, isKecil: false, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Biru", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
                
                items.append(Item(id: 20, name: "Happyland Mega Construction Set", category: "Solving Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [150000, 200000, 250000], buyPrice: 1400000, dimensions: [40, 40, 10], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Happyland Mega Construction Set", imageLove: "loveImage", jarakPengiriman: 3, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Kuning", minimumAgeInMonths: 3, maximumAgeInMonths: 6))
        
        items.append(Item(id: 21, name: "Baby Einstein Neptune’s Ocean Discovery Jumperoo", category: "Stimulating Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [40000, 70000, 120000], buyPrice: 1000000, dimensions: [10, 6, 5], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Baby Einstein Neptune’s Ocean Discovery Jumperoo", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Pink", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 22, name: "Giant Wooden Activity Cube", category: "Stimulating Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [150000, 180000, 200000], buyPrice: 1170000, dimensions: [32, 32, 32], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Giant Wooden Activity Cube", imageLove: "loveImage", jarakPengiriman: 6, isKecil: false, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Pink", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 23, name: "My First Slide - ELC", category: "Stimulating Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [100000, 130000, 160000], buyPrice: 1200000, dimensions: [90, 40, 60], dimensionUnit: Item.unit.cm.rawValue, imagePath: "My First Slide - ELC", imageLove: "loveImage", jarakPengiriman: 4, isKecil: false, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Biru", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 24, name: "Parklon Multifunction Combination Slide 5 in 1 – Blue", category: "Stimulating Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [200000, 240000, 300000], buyPrice: 2500000, dimensions: [2, 1, 2], dimensionUnit: Item.unit.m.rawValue, imagePath: "Parklon Multifunction Combination Slide 5 in 1 – Blue", imageLove: "loveImage", jarakPengiriman: 6, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Biru", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
        
        items.append(Item(id: 25, name: "Wooden Activity Table", category: "Stimulating Toys", onRental: [Date()], durationsInDays: [7,14,28], costs: [100000, 130000, 160000], buyPrice: 1000000, dimensions: [40, 40, 60], dimensionUnit: Item.unit.cm.rawValue, imagePath: "Wooden Activity Table", imageLove: "loveImage", jarakPengiriman: 3, isKecil: true, selectedDurationCode: 0, selectedStartingDate: Date(), numberInCart: 0, color: "Pink", minimumAgeInMonths: 2, maximumAgeInMonths: 4))
    }
    
    func siKecil() -> [Item]
       {
        return items.filter()
               {
                listItem in listItem.isKecil == true
           }
       }
    
    func dekatSaya() -> [Item]
       {
        return items.filter()
               {
                listItem in listItem.jarakPengiriman <= 5
           }
       }
    
    func getItemFromId(id: Int) -> [Item]
    {
        return items.filter(){
            listItem in listItem.id == id
        }
    }
}
