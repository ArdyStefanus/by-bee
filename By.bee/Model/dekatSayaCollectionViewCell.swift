//
//  dekatSayaCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 05/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class dekatSayaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconLoveDekat: UIImageView!
    @IBOutlet weak var imageDekat: UIImageView!
    @IBOutlet weak var textViewBUlan: UILabel!
    @IBOutlet weak var textViewKeterangan: UILabel!
    @IBOutlet weak var textViewHarga: UILabel!
    @IBOutlet weak var textViewTanggal: UILabel!
}
