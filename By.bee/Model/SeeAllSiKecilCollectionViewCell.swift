//
//  SeeAllSiKecilCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 06/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class SeeAllSiKecilCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var iconLoveSiKecil: UIImageView!
    @IBOutlet weak var imageMainSiKecil: UIImageView!
    @IBOutlet weak var textViewBulanSiKecil: UILabel!
    @IBOutlet weak var textViewKeteranganSiKecil: UILabel!
    @IBOutlet weak var textViewHargaSiKecil: UILabel!
    @IBOutlet weak var textViewTanggalSiKecil: UILabel!
}
