//
//  MainPageCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 05/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class MainPageCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var iconLove: UIImageView!
    @IBOutlet weak var imageMain: UIImageView!
    @IBOutlet weak var textBulan: UILabel!
    @IBOutlet weak var textKeterangan: UILabel!
    @IBOutlet weak var textHarga: UILabel!
    @IBOutlet weak var textTanggal: UILabel!
}
