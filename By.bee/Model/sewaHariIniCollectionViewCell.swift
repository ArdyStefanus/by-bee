//
//  sewaHariIniCollectionViewCell.swift
//  By.bee
//
//  Created by Ardy Stefanus Sunarno on 05/08/20.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import UIKit

class sewaHariIniCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconLoveSewa: UIImageView!
    @IBOutlet weak var imageSewa: UIImageView!
    @IBOutlet weak var textViewBulanSewa: UILabel!
    @IBOutlet weak var textViewKeteranganSewa: UILabel!
    @IBOutlet weak var textViewHargaSewa: UILabel!
    @IBOutlet weak var textViewTanggalSewa: UILabel!
}
