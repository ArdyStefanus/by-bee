//
//  Store.swift
//  By.bee
//
//  Created by Nicholas Ang on 05/08/2020.
//  Copyright © 2020 Ardy Stefanus Sunarno. All rights reserved.
//

import Foundation

struct Store
{
    var name: String
    var iconPath: String
    var listedItemsID: [Int]
}

class StoreManager
{
    
    var allItems = ItemManager().items
    var stores: [Store] = []
    init() {
        stores.append(Store(name: "Toko Sumber Makmur",
                            iconPath: "imagePrusutan",
                            listedItemsID: [1,2,3,4,5,6,7,8]))
        
        stores.append(Store(name: "Toko Makmur Jaya",
                            iconPath: "imagePrusutan",
                            listedItemsID: [9,10,11,12,13,14,15,16]))
        
        stores.append(Store(name: "Toko Sumber Jaya",
        iconPath: "imagePrusutan",
        listedItemsID: [17,18,19,20,21,22,23,24,25]))
    }
    
    func getStoreFromItemID(id:Int) -> Store {
        for storeIndex in 0...stores.count-1 {
            let itemsIDInStore = stores[storeIndex].listedItemsID
            
            for itemIndex in 0...itemsIDInStore.count-1 {
                if itemsIDInStore[itemIndex] == id {
                    return stores[storeIndex]
                    
                }
            }
        }
        print("Store Not Found")
        return stores[0]
    }
    
    func getItemsInCart() -> [(store: Store, items: [Item])] {
        
        var storeWithItemsInCart: [(store: Store, items: [Item])] = []
//        var allItems: [Item]
        for storeIndex in 0...stores.count-1 {
        
            let itemsIDInStore = stores[storeIndex].listedItemsID
            var isStoreinCart: Bool = false
            var itemsinCart: [Item] = []
            print(itemsIDInStore)
            print(itemsIDInStore.count)
            for itemIndex in 0...itemsIDInStore.count-1 {
                
                let itemToCheck = allItems[itemsIDInStore[itemIndex]-1 ]
                if itemToCheck.numberInCart > 0 {
                    isStoreinCart = true
                    itemsinCart.append(itemToCheck)
                }
            }
            
            if isStoreinCart {
                storeWithItemsInCart.append((store: stores[storeIndex], items: itemsinCart))
            }
        }
        print(storeWithItemsInCart)
        
        return storeWithItemsInCart
    }
    
    
}
